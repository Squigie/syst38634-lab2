package time;
/*
 * @author Luigi Agostino
 */

public class JavaMethods {
	
	public static void main (String args[]) {
		
		int small = SmallestNumber(37,25,29);
		System.out.println("The smallest number is " + small);
		
		int average = Average(25, 45, 65);
		System.out.println("The Average is " + average);
		
		MiddleOfString("350");
		VowelCount("w3resource");
		CharPosition("Java Exercises!", 10);
		CountUNICODE("w3rsource.com");
	}
	
	public static int SmallestNumber(int num1, int num2, int num3) {
		
		int smallNum = num1;
		
		if (num2 < smallNum) 
			smallNum = num2;
		if (num3 < smallNum)
			smallNum = num3;
		
		return smallNum;
	}
	
	public static int Average(int num1, int num2, int num3) {
		
		int average = (num1+num2+num3) / 3;
		return average;	
	}
	
	public static void MiddleOfString(String word) {
		int stringLen = word.length();
		
		if (stringLen % 2 != 0) {
			int middle = stringLen / 2;
			System.out.println("The Middle of the String is " + word.charAt(middle));
		}
		else {
			int middle1 = stringLen / 2;
			int middle2 = middle1 - 1;
			System.out.println("The Middle of the String is " + word.charAt(middle2) + " and " + word.charAt(middle1));
		}	
	}
	
	public static void VowelCount(String word) {
		int vowelCount = 0;
		
		for (int i =0; i < word.length(); i++) {
			char letter = word.charAt(i);
			if(letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u')
				vowelCount++;
		}
		
		System.out.println("There are " + vowelCount + " Vowels in the word.");	
	}
	
	public static void CharPosition (String word, int position) {
		System.out.println("The Character at Position " + position + " is " + word.charAt(position));
	}
	
	public static void CountUNICODE(String word) {
		int beginString = 0;
		int endString = word.length();
		int uniCount = word.codePointCount(beginString, endString);
		
		System.out.println("UNICode Count is " + uniCount);
	}
}
