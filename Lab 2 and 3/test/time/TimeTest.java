package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	//Lab 2 test cases
	//////////////////
	@Test
	public void testGetTotalSeconds() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The Time Provided Does not match the result", totalSeconds == 3661);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("The time provided is not valid");
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:59");
		assertTrue("The time provided does not match the result", totalSeconds == 59);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("The time provided is not valid");
	}
	
	//Lab 3 test Cases
	//////////////////
	
	@Test
	public void testGetTotalMilisecondsRegular() {
		int totalMiliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of miliseconds", totalMiliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMilisecondsException() {
		int totalMiliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		fail("Invalid number of miliseconds");
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMilisecondsBoundaryOut() {
		int totalMiliseconds = Time.getTotalMilliseconds("12:05:50:1000");
		fail("Invalid number of miliseconds");
	}
	
	@Test
	public void testGetTotalMilisecondsBoundaryIn() {
		int totalMiliseconds = Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("Invalid number of miliseconds", totalMiliseconds == 999);
	}

}
